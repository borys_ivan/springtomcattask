package com.example.message.controller;

import com.example.message.entity.AjaxResponseBody;
import com.example.message.entity.Message;
import com.example.message.entity.SearchCriteria;
import com.example.message.service.MessageService;
import com.example.message.validate.MessageValidator;
import com.example.message.validate.SaerchCriteriaValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class MessageController {

    @Autowired
    private MessageValidator messageValidator;

    @Autowired
    private SaerchCriteriaValidator saerchCriteriaValidator;

    @Autowired
    private MessageService messageService;


    @GetMapping("/list")
    public String listPostcodes(Model theModel) {
        List<Message> theMessage = messageService.getMessages();
        theModel.addAttribute("message", theMessage);
        theModel.addAttribute("test", "sdcsdcsdcsdcsdcsdcsdcsdcsdcsdcsd");
        return "list-message";
    }


    @GetMapping("/")
    public String showFormForAdd(Model theModel) {
        Message theMessage = new Message();
        theModel.addAttribute("massage", theMessage);

        return "add_message";
    }

    @PostMapping("/saveMassage")
    public String saveMessage(@Valid @ModelAttribute("AddedMessage") Message theMessage, BindingResult bindingResult, Model model) {

        messageValidator.validate(theMessage, bindingResult);

        if (bindingResult.hasErrors()) {
            return "message_edit";
        }

        messageService.saveMessage(theMessage);

        model.addAttribute("phone", theMessage.getPhone());
        model.addAttribute("email", theMessage.getEmail());
        model.addAttribute("message", theMessage.getMessage());
        return "added_success";
    }


    @GetMapping("/search")
    public String search() {

        return "ajax";
    }


    @PostMapping("/api/search")
    public ResponseEntity<?> searchMessage(@Valid @RequestBody SearchCriteria search,BindingResult bindingResult, Errors errors) {

        AjaxResponseBody result=new AjaxResponseBody();

        saerchCriteriaValidator.validate(search,bindingResult);

        if (errors.hasErrors()) {

            result.setMsg(errors.getAllErrors()
                    .stream().map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));

            return ResponseEntity.badRequest().body(result);

        }

        List<Message> messages = messageService.findByPhone(search.getPhone());
        if (messages.isEmpty()) {
            result.setMsg("no message found!");
        } else {
            result.setMsg("success");
        }
        result.setResult(messages);

        return ResponseEntity.ok(result);
    }


    }

