package com.example.message.entity;

import java.util.List;

public class AjaxResponseBody {

    String msg;
    List<Message> result;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Message> getResult() {
        return result;
    }

    public void setResult(List<Message> result) {
        this.result = result;
    }
}
