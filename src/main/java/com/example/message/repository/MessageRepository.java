package com.example.message.repository;

import com.example.message.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface MessageRepository extends JpaRepository<Message, Integer> {

    List<Message> findByPhone(String phone);
}
