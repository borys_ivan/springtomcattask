package com.example.message.service;

import com.example.message.entity.Message;
//import spring.entity.Message;

import java.util.List;

public interface MessageService {

    public List <Message> getMessages();

    void saveMessage(Message theMessage);

    List<Message> findByPhone(String phone);
}
