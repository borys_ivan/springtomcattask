package com.example.message.service;

import com.example.message.entity.Message;
import com.example.message.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageRepository messageRepository;

    @Override
    @Transactional
    public List<Message> getMessages() {
        return messageRepository.findAll(Sort.by(Sort.Direction.DESC, "id"));
    }

    @Override
    public void saveMessage(Message theMessage) {
        messageRepository.save(theMessage);
    }

    public List<Message> findByPhone(String phone) {
        return messageRepository.findByPhone(phone);
    }

}

