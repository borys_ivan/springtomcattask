package com.example.message.validate;

import com.example.message.entity.SearchCriteria;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class SaerchCriteriaValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        //return false;
        return SearchCriteria.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"phone","phone can't empty!");

    }
}
