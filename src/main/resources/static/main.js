$(document).ready(function () {

    $("#search-form").submit(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();

        fire_ajax_submit();

    });

});

function fire_ajax_submit() {

    var search = {}
    search["phone"] = $("#phone").val();

    $("#btn-search").prop("disabled", true);

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/api/search",
        data: JSON.stringify(search),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var requestTable = "<table class='table table-striped table-bordered'>";

            $.each(data.result, function (index, value) {
                requestTable += "<tr><td>" + value.email + "</td><td>" + value.phone + "</td></tr>" +
                    "<tr><td colspan='2'>" + value.message + "</td></tr>";

            });

            if (data.result.length === 0) {

                $('.panel-body').html(requestTable += "<tr><td colspan='2'>" + data["msg"] + "</td></tr>");
                requestTable += "</table>";

            } else {

                requestTable += "</table>";
                $('.panel-body').html(requestTable);

            }

            console.log("SUCCESS : ", data);
            $("#btn-search").prop("disabled", false);

        },
        error: function (e) {

            var requestTable = "<table class='table table-striped table-bordered'>";
            if (e.responseText.result == null) {
                $('.panel-body').html(requestTable += "<tr><td colspan='2'>Write something</td></tr>");
            }
            requestTable += "</table>";

            console.log("ERROR : ", e);
            $("#btn-search").prop("disabled", false);

        }
    });

}
